package com.project.carunlock

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.project.carunlock.databinding.ActivityMainBinding
import com.project.carunlock.fragments.SettingsFragment
import com.project.carunlock.fragments.StatsFragment
import com.project.carunlock.fragments.StatsJavaFragment
import com.project.carunlock.fragments.StudyFragment

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

    }

    override fun onStart() {
        super.onStart()

        changeFragment(StudyFragment())

        binding.study.setOnClickListener {
            changeFragment(StudyFragment())
        }
        binding.stats.setOnClickListener {
            changeFragment(StatsJavaFragment())
        }
        binding.settings.setOnClickListener {
            changeFragment(SettingsFragment())
        }

    }

    private fun changeFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainerView, fragment)
            setReorderingAllowed(true)
            addToBackStack(null) // name can be null
            commit()
        }
    }


}
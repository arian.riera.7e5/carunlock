package com.project.carunlock.fragments;

import android.os.Bundle;
import android.renderscript.ScriptGroup;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Pie;
import com.google.android.material.progressindicator.BaseProgressIndicator;
import com.project.carunlock.R;
import com.project.carunlock.databinding.FragmentStatsBinding;

import java.util.ArrayList;
import java.util.List;

public class StatsJavaFragment extends Fragment {

    FragmentStatsBinding binding = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = FragmentStatsBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Pie pie = AnyChart.pie();

        List<DataEntry> dataEntries = new ArrayList<>();
        dataEntries.add(new ValueDataEntry("John", 200));
        dataEntries.add(new ValueDataEntry("Jake", 400));
        dataEntries.add(new ValueDataEntry("Peter", 600));

        pie.data(dataEntries);
        pie.title("Hola");
        System.out.println("------------------------ ------------------- -----------------------");
        for (DataEntry data2 : dataEntries) {
            System.out.println(data2.generateJs());
        }

        AnyChartView anyChartView = (AnyChartView) binding.anyChartView;
        anyChartView.setChart(pie);

    }
}

package com.project.carunlock.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.project.carunlock.databinding.FragmentStatsBinding

import com.anychart.chart.common.dataentry.ValueDataEntry
import com.anychart.chart.common.dataentry.DataEntry
import com.anychart.AnyChart
import com.anychart.AnyChartView
import com.anychart.charts.Pie

class StatsFragment : Fragment() {

    private lateinit var binding: FragmentStatsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentStatsBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val pie : Pie = AnyChart.pie()

//        val data: MutableList<DataEntry> = ArrayList()
//        data.add(ValueDataEntry("Acertadas", 326))
//        data.add(ValueDataEntry("Falladas", 120))

        val data: MutableList<DataEntry> = arrayListOf(ValueDataEntry("Acertadas", 326), ValueDataEntry("Falladas", 120))
        pie.data(data)

        val anyChartView = binding.anyChartView as AnyChartView
        anyChartView.setChart(pie)

    }

}